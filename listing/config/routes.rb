Rails.application.routes.draw do
  mount Datadisplay::Engine, at: "/display"

  resources :users
  resources :books

  root to: 'books#index'
end
